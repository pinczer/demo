﻿using Solwit.Repository.Domain;
using Solwit.Repository.Repositories;
using Solwit.Services.Extensions;
using System;
using System.Collections.Generic;

namespace Solwit.Services
{
    public class DictionaryService : IDictionaryService
    {
        private IDictionariesRepository _repository;

        public DictionaryService(IDictionariesRepository repository)
        {
            _repository = repository;
        }

        public List<DictionaryItem> Get(DictionaryType dictionaryType)
        {
            switch (dictionaryType)
            {
                case DictionaryType.Priority:
                    return _repository.GetPriorities().ToDictionaryItemList();
                case DictionaryType.OrderType:
                    return _repository.GetOrderTypes().ToDictionaryItemList();
                default:
                    break;
            }
            throw new ArgumentException();
        }

        
    }
}