﻿var datasources = function() {
    var crudServiceBaseUrl = window.location.origin;

    return {
        orders: new kendo.data.DataSource({
            schema: {
                model: {
                    id: "Id",
                    fields: {
                        Title: { type: "string" },
                        Comment: { type: "string" },
                        Priority: { type: "string" },
                        OrderType: { type: "string" }
                    }
                }
            },
            transport: {
                read: {
                    url: serviceUrls.orders,
                    dataType: "json"
                },
            }
        }),
        prioritiesDict: new kendo.data.DataSource({
            transport: {
                read: {
                    url: serviceUrls.prioritiesDict,
                    dataType: "json"
                }
            }
        }),
        orderTypesDict: new kendo.data.DataSource({
            transport: {
                read: {
                    url: serviceUrls.orderTypesDict,
                    dataType: "json"
                }
            }
        })
    }
}();