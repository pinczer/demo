﻿using System.Collections.Generic;
using System.Linq;
using Solwit.Models;
using Solwit.Repository.Domain;
using Solwit.Repository.Repositories;

namespace Solwit.Services
{
    public class OrdersService : IOrdersService
    {
        IOrdersRepository _repository;

        public OrdersService(IOrdersRepository repository)
        {
            _repository = repository;
        }

        public void Add(OrderItemViewModel order)
        {
            Order itemToAdd = MapperConfig.Mapper.Map<OrderItemViewModel, Order>(order);
            _repository.Add(itemToAdd);
        }

        public List<OrderGridItemViewModel> Get()
        {
            List<Order> orders = _repository.Get();
            return orders.Select(o => MapperConfig.Mapper.Map<Order, OrderGridItemViewModel>(o)).ToList();
        }
    }
}