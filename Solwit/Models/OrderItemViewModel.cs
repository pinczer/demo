﻿namespace Solwit.Models
{
    public class OrderItemViewModel : OrderViewModel
    {
        public int? PriorityId { get; set; }
        public int? OrderTypeId { get; set; }
    }
}