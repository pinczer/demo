﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solwit.Repository.Domain;

namespace Solwit.Repository.Repositories
{
    public class OrdersRepository : IOrdersRepository
    {
        DomainContext _domainContext;

        public OrdersRepository()
        {
            _domainContext = new DomainContext();
        }

        public void Add(Order order)
        {
            _domainContext.Orders.Add(order);
            _domainContext.SaveChanges();
        }

        public List<Order> Get()
        {
            return _domainContext.Orders.ToList();
        }
    }
}
