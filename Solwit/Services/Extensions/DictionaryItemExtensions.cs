﻿using Solwit.Repository.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Solwit.Services.Extensions
{
    public static class DictionaryItemExtensions
    {
        public static List<DictionaryItem> ToDictionaryItemList<T>(this IEnumerable<T> list) where T : DictionaryItem
        {
            return list.Select(t => (DictionaryItem)t).ToList();
        }
    }
}