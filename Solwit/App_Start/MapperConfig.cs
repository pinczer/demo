﻿using AutoMapper;
using Solwit.Models;
using Solwit.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Solwit
{
    public class MapperConfig
    {
        private static IMapper _mapper;

        public static IMapper Mapper
        {
            get
            {
                if (_mapper == null)
                {
                    Initialize();
                }
                return _mapper;
            }
        }

        public static void Initialize()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<OrderItemViewModel, Order>();
                cfg.CreateMap<Order, OrderGridItemViewModel>()
                    .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.OrderType != null ? src.OrderType.Name : null))
                    .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority != null ? src.Priority.Name : null));

            });

            _mapper = config.CreateMapper();
        }
    }
}