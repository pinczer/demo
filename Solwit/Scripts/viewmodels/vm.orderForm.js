﻿function orderFormVieModel($selector) {

    var model = kendo.observable({
        order: {
            id: 0,
            title: null,
            comment: null,
            priorityId: -1,
            orderTypeId: -1
        },
        prioritiesSource: datasources.prioritiesDict,
        orderTypesSource: datasources.orderTypesDict,
        onPriorityChange: function (e) {
            this.order.priorityId = e.sender.value();
        },
        isSuuccessAlertVisible: false,
        save: function (e) {
   

            e.preventDefault();

            if (kendoValidator.validate()) {

                var order = this.get('order').toJSON();
                $.post(serviceUrls.addOrder(),
                    order,
                    function (data, status) {
                        model.set('isSuuccessAlertVisible', true);
                    });
            }
        }

    })

    $selector.kendoValidator({
        validateOnBlur: false // Disable the default validation on blur
    });

    // Get the validator instance
    var kendoValidator = $selector.getKendoValidator();

    return {
        getModel: function () {
            return model;
        }
    }
};