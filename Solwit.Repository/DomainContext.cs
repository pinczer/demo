﻿using Solwit.Repository.Domain;
using System.Data.Entity;

namespace Solwit.Repository
{
    public class DomainContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderType> OrderTypes { get; set; }
        public DbSet<Priority> Priorities { get; set; }
    }
}
