﻿namespace Solwit.Models
{
    public abstract class OrderViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
    }
}