namespace Solwit.Repository.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Solwit.Repository.DomainContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Solwit.Repository.DomainContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            context.Priorities.AddOrUpdate(
              p => p.Id,
              new Domain.Priority { Id = 1, Name = "Niski" },
              new Domain.Priority { Id = 2, Name = "�redni" },
              new Domain.Priority { Id = 3, Name = "Wysoki" }
            );

            context.OrderTypes.AddOrUpdate(
              p => p.Id,
              new Domain.OrderType { Id = 1, Name = "B��d" },
              new Domain.OrderType { Id = 2, Name = "Rozszerzenie" },
              new Domain.OrderType { Id = 3, Name = "Pytanie" }
            );
        }
    }
}
