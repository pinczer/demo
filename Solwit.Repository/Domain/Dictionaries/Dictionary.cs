﻿namespace Solwit.Repository.Domain
{
    public abstract class DictionaryItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
