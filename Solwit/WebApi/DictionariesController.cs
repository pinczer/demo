﻿using Solwit.Repository.Domain;
using Solwit.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace Solwit.WebApi
{
    public class DictionariesController : ApiController
    {
        private IDictionaryService _dictionaryService;

        public DictionariesController(IDictionaryService dictionaryService)
        {
            _dictionaryService = dictionaryService;
        }
        
        [HttpGet]
        [Route("api/dictionaries/{dictionaryType}")]
        public List<DictionaryItem> Get(string dictionaryType)
        {
            DictionaryType type;
            
            if(!Enum.TryParse(dictionaryType, true, out type) || !Enum.IsDefined(typeof(DictionaryType), type))
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }  

            return _dictionaryService.Get(type);
        }
        
    }
}
