﻿var serviceUrls = function () {

    var crudServiceBaseUrl = window.location.origin;

    return {
        orders: function () {
            return crudServiceBaseUrl + "/api/orders";
        },
        addOrder: function () {
            return crudServiceBaseUrl + "/api/orders";
        },
        prioritiesDict: function () {
            return crudServiceBaseUrl + "/api/dictionaries/priority";
        },
        orderTypesDict: function () {
            return crudServiceBaseUrl + "/api/dictionaries/orderType";
        }
    }
}();