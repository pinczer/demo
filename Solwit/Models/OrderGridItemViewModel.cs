﻿namespace Solwit.Models
{
    public class OrderGridItemViewModel : OrderViewModel
    {
        public string Priority { get; set; }
        public string OrderType { get; set; }
    }
}