namespace Solwit.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initializedatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Comment = c.String(),
                        PriorityId = c.Int(nullable: false),
                        OrderTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderTypes", t => t.OrderTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Priorities", t => t.PriorityId, cascadeDelete: true)
                .Index(t => t.PriorityId)
                .Index(t => t.OrderTypeId);
            
            CreateTable(
                "dbo.OrderTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Priorities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "PriorityId", "dbo.Priorities");
            DropForeignKey("dbo.Orders", "OrderTypeId", "dbo.OrderTypes");
            DropIndex("dbo.Orders", new[] { "OrderTypeId" });
            DropIndex("dbo.Orders", new[] { "PriorityId" });
            DropTable("dbo.Priorities");
            DropTable("dbo.OrderTypes");
            DropTable("dbo.Orders");
        }
    }
}
