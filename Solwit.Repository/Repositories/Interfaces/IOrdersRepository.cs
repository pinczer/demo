﻿using Solwit.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solwit.Repository.Repositories
{
    public interface IOrdersRepository
    {
        void Add(Order order);
        List<Order> Get();
    }
}
