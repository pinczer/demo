namespace Solwit.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullabledictionaries : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "OrderTypeId", "dbo.OrderTypes");
            DropForeignKey("dbo.Orders", "PriorityId", "dbo.Priorities");
            DropIndex("dbo.Orders", new[] { "PriorityId" });
            DropIndex("dbo.Orders", new[] { "OrderTypeId" });
            AlterColumn("dbo.Orders", "PriorityId", c => c.Int());
            AlterColumn("dbo.Orders", "OrderTypeId", c => c.Int());
            CreateIndex("dbo.Orders", "PriorityId");
            CreateIndex("dbo.Orders", "OrderTypeId");
            AddForeignKey("dbo.Orders", "OrderTypeId", "dbo.OrderTypes", "Id");
            AddForeignKey("dbo.Orders", "PriorityId", "dbo.Priorities", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "PriorityId", "dbo.Priorities");
            DropForeignKey("dbo.Orders", "OrderTypeId", "dbo.OrderTypes");
            DropIndex("dbo.Orders", new[] { "OrderTypeId" });
            DropIndex("dbo.Orders", new[] { "PriorityId" });
            AlterColumn("dbo.Orders", "OrderTypeId", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "PriorityId", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "OrderTypeId");
            CreateIndex("dbo.Orders", "PriorityId");
            AddForeignKey("dbo.Orders", "PriorityId", "dbo.Priorities", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Orders", "OrderTypeId", "dbo.OrderTypes", "Id", cascadeDelete: true);
        }
    }
}
