﻿namespace Solwit.Repository.Domain
{
    public class Order
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }

        public int? PriorityId { get; set; }
        public virtual Priority Priority { get; set; }

        public int? OrderTypeId { get; set; }
        public virtual OrderType OrderType { get; set; }

        public Order()
        {

        }
    }
}
