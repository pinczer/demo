﻿using Solwit.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solwit.Repository.Repositories
{
    public interface IDictionariesRepository
    {
        List<Priority> GetPriorities();
        List<OrderType> GetOrderTypes();
    }
}
