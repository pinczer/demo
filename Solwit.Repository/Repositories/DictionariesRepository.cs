﻿using Solwit.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solwit.Repository.Repositories
{
    public class DictionariesRepository : IDictionariesRepository
    {
        DomainContext _domainContext;

        public DictionariesRepository()
        {
            _domainContext = new DomainContext();
        }

        public List<Priority> GetPriorities()
        {
            return _domainContext.Priorities.ToList();
        }

        public List<OrderType> GetOrderTypes()
        {
            return _domainContext.OrderTypes.ToList();
        }
    }
}
