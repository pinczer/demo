﻿using Solwit.Models;
using Solwit.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace Solwit.WebApi
{
    public class OrdersController : ApiController
    {
        private IOrdersService _ordersService;

        public OrdersController(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        // GET api/<controller>
        [HttpGet]
        [Route("api/orders")]
        public List<OrderGridItemViewModel> Get()
        {
            return _ordersService.Get();
        }

        [HttpPost]
        [Route("api/orders")]
        public void Post([FromBody]OrderItemViewModel order)
        {
            _ordersService.Add(order);
        }

    }
}