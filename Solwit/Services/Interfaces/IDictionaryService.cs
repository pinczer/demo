﻿using Solwit.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solwit.Services
{
    public interface IDictionaryService
    {
        List<DictionaryItem> Get(DictionaryType dictionaryType);
    }
}
