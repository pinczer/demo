﻿using Solwit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solwit.Services
{
    public interface IOrdersService
    {
        void Add(OrderItemViewModel order);
        List<OrderGridItemViewModel> Get();
    }
}
